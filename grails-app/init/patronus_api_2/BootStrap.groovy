package patronus_api_2

class BootStrap {

    def init = { servletContext ->
        def admin = Role.findOrSaveWhere(authority: 'ROLE_ADMIN')
        def employee = Role.findOrSaveWhere(authority: 'ROLE_EMPLOYEE')
        def admin_patronus = Role.findOrSaveWhere(authority: 'ROLE_ADMIN_PATRONUS')
    }
    def destroy = {
    }
}
