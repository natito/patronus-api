import patronus_api_2.UserPasswordEncoderListener
import patronus_api_2.BackendAuthenticationProvider
// Place your Spring DSL code here
beans = {
    userPasswordEncoderListener(UserPasswordEncoderListener)
    backendAuthenticationProvider(BackendAuthenticationProvider)
}
