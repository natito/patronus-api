package patronus_api_2

import grails.plugin.springsecurity.annotation.Secured
import grails.plugins.rest.client.RestBuilder

@Secured(['ROLE_EMPLOYEE', 'ROLE_ADMIN'])
class EventsController {
    def rest = new RestBuilder()
    def tokenService = new TokenService()

    def close() {
        def resp = rest.post("https://fierce-caverns-84695.herokuapp.com/users/${params.userId}/events/${params.eventId}/close") {
            contentType("application/json")
            header("Authorization", tokenService.getToken())
        }

        render(contentType: "text/json", text: resp.json, status: resp.status)
    }

    def free() {
        def resp = rest.post("https://fierce-caverns-84695.herokuapp.com/users/${params.userId}/events/${params.eventId}/free") {
            contentType("application/json")
            header("Authorization", tokenService.getToken())
        }

        render(contentType: "text/json", text: resp.json, status: resp.status)
    }

    def localizations() {
        def resp = rest.get("https://fierce-caverns-84695.herokuapp.com/users/${params.userId}/events/${params.eventId}/localizations") {
            contentType("application/json")
            header("Authorization", tokenService.getToken())
        }

        render(contentType: "text/json", text: resp.json, status: resp.status)
    }
}
