package patronus_api_2

class UrlMappings {

    static mappings = {
        "/$controller/$action?/$id?(.$format)?"{
            constraints {
                // apply constraints here
            }
        }

        "/users/$clientId"(controller: "user", action: "getUser")

        "/users/$userId/events/$eventId/close"(controller: "events", action: "close")
        "/users/$userId/events/$eventId/free"(controller: "events", action: "free")
        "/users/$userId/events/$eventId/localizations"(controller: "events", action: "localizations")

        "/company/$company/users/$user_id/assign"(controller: "liveAlerts", action: "assign")
        "/company/$company/users/$user_id/assign/$event_id"(controller: "liveAlerts", action: "assignFixed")

        "/company/$company/user/$user_id/delete"(controller: "admin", action: "delete")
        "/company/$company/user/$user_id/update"(controller: "admin", action: "update")
        "/company/$company/user/$user_id/new"(controller: "admin", action: "create")
        "/company/$company/clients/$client_id"(controller: "admin", action: "listClient")
        "/company/$company/clients/$client_id/delete"(controller: "admin", action: "deleteClient")
        "/company/$company/clients/$client_id/new"(controller: "admin", action: "createClient")

        "/"(controller:"main")
        "500"(view:'/error')
        "404"(view:'/notFound')
    }
}
