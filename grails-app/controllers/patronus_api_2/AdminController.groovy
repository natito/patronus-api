package patronus_api_2

import com.fasterxml.jackson.annotation.JsonProperty
import grails.converters.JSON
import grails.plugins.rest.client.RestBuilder

import javax.servlet.http.Cookie
import grails.plugin.springsecurity.annotation.Secured

@Secured(['ROLE_ADMIN'])
class AdminController {
    def rest = new RestBuilder()
    def tokenService = new TokenService()
    def errorMappings = [
            "User already exists"                                                                                  : "Ya existe un usuario registrado con estos datos",
            "User not found"                                                                                       : "Los datos ingresados son incorrectos. Por favor, ingresalos nuevamente",
            "Wrong Password"                                                                                       : "Los datos ingresados son incorrectos. Por favor, ingresalos nuevamente",
            "Password is mandatory"                                                                                : "Contraseña es obligatoria",
            "Mail is mandatory"                                                                                    : "Mail es obligatorio",
            "Document Type is mandatory"                                                                           : "Tipo de Documento es obligatorio",
            "Document is mandatory"                                                                                : "Número de Documento es obligatorio",
            "Client Number is mandatory"                                                                           : "Número de Afiliado es obligatorio",
            "Area Code is mandatory"                                                                               : "Código de Área es obligatorio",
            "Phone Number is mandatory"                                                                            : "Número de Teléfono es obligatorio",
            "First Name is mandatory"                                                                              : "Nombre es obligatorio",
            "Last Name is mandatory"                                                                               : "Apellido es obligatorio",
            "Longitude is mandatory"                                                                               : "Longitud es obligatorio",
            "Latitude is mandatory"                                                                                : "Latitud es obligatorio",
            "Company name is mandatory"                                                                            : "Empresa es obligatorio",
            "Cuit is mandatory"                                                                                    : "CUIT es obligatorio",
            "Legajo is mandatory"                                                                                  : "Legajo es obligatorio",
            "Document is not whitelisted for given client number"                                                  : "El tipo y número de documento no está registrado bajo el número de afiliado ingresado",
            "Password should have at least 8 characters and one of lowercase, uppercase, number, special character": "La contraseña debe tener como mínimo 8 caracteres, una minúscula, una mayúscula, un número y un caracter especial (@#\$%!)",
            "Document parameter should be numeric and between 7 and 8 digits"                                      : "Número de Documento debe ser un número entre 7 y 8 dígitos",
            "Client Number should be numeric"                                                                      : "Número de Afiliado debe contener sólo números",
            "Area Code should be numeric"                                                                          : "Código de Área debe contener sólo números",
            "Phone Number should be numeric and between 6 and 8 digits"                                            : "Número de Teléfono debe contener sólo números y contener 8 dígitos",
            "Mail parameter is not a valid e-mail address"                                                         : "El Mail ingresado no es una dirección de correo válida",
            "Cuit is invalid"                                                                                      : "CUIT no tiene un formato válido"
    ]

    def index() {
        if (request.cookies?.length == 0) {
            response.addCookie(LoginController.createCookie("user_id", params.user_id))
            response.addCookie(LoginController.createCookie("cuit", params.company))
            response.addCookie(LoginController.createCookie("user_type", 'admin'))
        }

        def employees = getEmployees(params.company)

        def resp_name = rest.get("https://fierce-caverns-84695.herokuapp.com/companies/${params.company}/users/${params.user_id}") {
            header("Authorization", tokenService.getToken())
        }
        def user_name = resp_name.json

        render(view: "/admin/index", model: [
                employees: employees.users,
                user     : [
                        id     : params.user_id,
                        company: params.company,
                        name   : user_name.first_name
                ],
                company  : params.company
        ])
    }

    def delete() {
        def resp = rest.delete("https://fierce-caverns-84695.herokuapp.com/companies/${params.company}/users/${params.user_id}") {
            contentType("application/json")
            header("Authorization", tokenService.getToken())
        }

        if (resp.json?.errors) {
            resp.json.errors = resp.json.errors.collect { err ->
                errorMappings[err] ?: err
            }
        }

        render(contentType: "text/json", text: resp.json, status: resp.status)
    }

    def update() {
        def body = JSON.parse(new InputStreamReader(request.getInputStream()))

        def resp = rest.put("https://fierce-caverns-84695.herokuapp.com/companies/${params.company}/users/${params.user_id}") {
            contentType("application/json")
            json {
                first_name = body.first_name
                last_name = body.last_name
                mail = body.mail
            }
            header("Authorization", tokenService.getToken())
        }

        if (resp.json?.errors) {
            resp.json.errors = resp.json.errors.collect { err ->
                errorMappings[err] ?: err
            }
        }

        render(contentType: "text/json", text: resp.json, status: resp.status)
    }

    def create() {
        def body = JSON.parse(new InputStreamReader(request.getInputStream()))

        def resp = rest.post("https://fierce-caverns-84695.herokuapp.com/companies/${params.company}/users") {
            contentType("application/json")
            json {
                legajo = body.legajo
                first_name = body.first_name
                last_name = body.last_name
                mail = body.mail
                password = body.password
            }
            header("Authorization", tokenService.getToken())
        }

        if (resp.json?.errors) {
            resp.json.errors = resp.json.errors.collect { err ->
                errorMappings[err] ?: err
            }
        }

        render(contentType: "text/json", text: resp.json, status: resp.status)
    }

    def createClient() {
        def body = JSON.parse(new InputStreamReader(request.getInputStream()))

        def resp = rest.post("https://fierce-caverns-84695.herokuapp.com/companies/${params.company}/clients/${params.client_id}") {
            contentType("application/json")
            json {
                document = body.document
                document_type = body.document_type
            }
            header("Authorization", tokenService.getToken())
        }

        if (resp.json?.errors) {
            resp.json.errors = resp.json.errors.collect { err ->
                errorMappings[err] ?: err
            }
        }

        render(contentType: "text/json", text: resp.json, status: resp.status)
    }

    def getEmployees(company) {
        def resp = rest.get("https://fierce-caverns-84695.herokuapp.com/companies/${company}/users") {
            contentType("application/json")
            header("Authorization", tokenService.getToken())
        }

        return resp.json
    }

    def listClient() {
        def resp = rest.get("https://fierce-caverns-84695.herokuapp.com/companies/${params.company}/clients/${params.client_id}") {
            contentType("application/json")
            header("Authorization", tokenService.getToken())
        }

        render(contentType: "text/json", text: resp.json, status: resp.status)
    }

    def deleteClient() {
        def body = JSON.parse(new InputStreamReader(request.getInputStream()))

        def resp = rest.delete("https://fierce-caverns-84695.herokuapp.com/companies/${params.company}/clients/${params.client_id}" +
                "?document=${body.document}&document_type=${body.document_type}") {
            contentType("application/json")
            header("Authorization", tokenService.getToken())
        }

        if (resp.json?.errors) {
            resp.json.errors = resp.json.errors.collect { err ->
                errorMappings[err] ?: err
            }
        }

        render(contentType: "text/json", text: resp.json, status: resp.status)
    }
}
