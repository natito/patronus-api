package patronus_api_2

import grails.converters.JSON
import grails.plugins.rest.client.RestBuilder
import grails.plugin.springsecurity.annotation.Secured
import javax.servlet.http.Cookie

@Secured(['ROLE_EMPLOYEE', 'ROLE_ADMIN'])
class LiveAlertsController {
    def rest = new RestBuilder()
    def tokenService = new TokenService()

    def index() {
        if (request.cookies?.length == 0) {
            response.addCookie(LoginController.createCookie("user_id", params.user_id))
            response.addCookie(LoginController.createCookie("cuit", params.company))
            response.addCookie(LoginController.createCookie("user_type", params.user_type))
        }

        def event = [:]
        def client = [:]

        if (params.user_type != 'admin') {
            event = getEvent(params.company, params.user_id)

            if (event) {
                event.date = formatDate(event.date)
                def resp = rest.get("https://fierce-caverns-84695.herokuapp.com/users/${event.client_id}") {
                    header("Authorization", tokenService.getToken())
                }
                client = resp.json
            }

        }

        if (event.isEmpty()) {
            event = [
                    id           : "",
                    recording_id : "",
                    client_id    : "",
                    localizations: []
            ]
        }

        def resp_name = rest.get("https://fierce-caverns-84695.herokuapp.com/companies/${params.company}/users/${params.user_id}") {
            header("Authorization", tokenService.getToken())
        }
        def user_name = resp_name.json

        render(view: "/liveAlerts/index", model: [
                event: event,
                user : [
                        id     : params.user_id,
                        company: params.company,
                        type   : params.user_type,
                        name   : user_name.first_name
                ],
                client:client
        ])
    }

    def formatDate(eventDate) {
        def datef = Date.parse("yyyy-MM-dd'T'HH:mm:SS", eventDate)
        return datef.format('dd/MM/yyyy HH:mm:SS')
    }

    def assign() {
        def event = getEvent(params.company, params.user_id)
        def status = event.isEmpty() ? 204 : 200

        render(contentType: "text/json", text: event as JSON, status: status)
    }

    def assignFixed() {
        def event = getEventFixed(params.company, params.user_id, params.event_id)
        render(contentType: "text/json", text: event)
    }

    def getEvent(company, userId) {
        def resp = rest.post("https://fierce-caverns-84695.herokuapp.com/companies/${company}/users/${userId}/assign") {
            contentType("application/json")
            header("Authorization", tokenService.getToken())
        }

        if(resp.status == 204){
            return [:]
        }

        def eventId = resp.json.id
        def clientId = resp.json.client_id

        def event = rest.get("https://fierce-caverns-84695.herokuapp.com/users/${clientId}/events/${eventId}") {
            contentType("application/json")
            header("Authorization", tokenService.getToken())
        }

        return event.json
    }

    def getEventFixed(company, userId, eventId) {
        def resp = rest.post("https://fierce-caverns-84695.herokuapp.com/companies/${company}/users/${userId}/assign/${eventId}") {
            contentType("application/json")
            header("Authorization", tokenService.getToken())
        }

        if(resp.json) {

            def clientId = resp.json.client_id

            def event = rest.get("https://fierce-caverns-84695.herokuapp.com/users/${clientId}/events/${eventId}") {
                contentType("application/json")
                header("Authorization", tokenService.getToken())
            }

            return event.json
        }

        else return "{}"
    }

}
