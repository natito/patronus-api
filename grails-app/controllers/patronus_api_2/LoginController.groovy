package patronus_api_2

import grails.plugins.rest.client.RestBuilder
import org.springframework.util.LinkedMultiValueMap
import org.springframework.util.MultiValueMap

import javax.servlet.http.Cookie

class LoginController extends grails.plugin.springsecurity.LoginController {

    def rest = new RestBuilder()

    def index() {}

    def auth() {}

    static Cookie createCookie(String name, String value){
        Cookie cookie = new Cookie(name, value)

        cookie.path = "/";

        return cookie
    }
}
