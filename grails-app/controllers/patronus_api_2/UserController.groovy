package patronus_api_2
import grails.plugin.springsecurity.annotation.Secured
import grails.plugins.rest.client.RestBuilder

@Secured(['ROLE_EMPLOYEE', 'ROLE_ADMIN'])
class UserController {
    def rest = new RestBuilder()
    def tokenService = new TokenService()

    def getUser() {

        def resp = rest.get("https://fierce-caverns-84695.herokuapp.com/users/${params.clientId}") {
            header("Authorization", tokenService.getToken())
        }

        render(contentType: "text/json", text: resp.json, status: resp.status)
    }
}
