package patronus_api_2

import grails.plugin.springsecurity.annotation.Secured

@Secured(['ROLE_EMPLOYEE','ROLE_ADMIN'])
class MainController {
    def springSecurityService
    def index() {
        def userLoggedIn = springSecurityService.principal
        switch(userLoggedIn.authorities[0].authority) {
            case "ROLE_EMPLOYEE":
                redirect(controller: "liveAlerts", params: [
                        "user_id": userLoggedIn.legajo,
                        "company": userLoggedIn.cuit,
                        "user_type": "employee"
                ])
                break
            case "ROLE_ADMIN":
                redirect(controller: "admin", params: [
                        "user_id": userLoggedIn.legajo,
                        "company": userLoggedIn.cuit
                ])
                break


        }
    }
}
