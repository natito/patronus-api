<asset:stylesheet src="main_operador.css"/>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Patronus</title>
    <link rel="icon" type="image/x-ico" href="/assets/favicon.ico">
    <meta name="viewport" content="initial-scale=1,maximum-scale=1,user-scalable=no"/>
    <script src="https://api.mapbox.com/mapbox-gl-js/v1.12.0/mapbox-gl.js"></script>
    <link href="https://api.mapbox.com/mapbox-gl-js/v1.12.0/mapbox-gl.css" rel="stylesheet"/>
    <style>
    body {
        margin: 0;
        padding: 0;
        background-color: #FFFFFF;
    }

    #map {
        position: absolute;
        top: 0;
        bottom: 0;
        left: 20%;
        width: 80%;
    }

    .recording {
        width: 100%;
        height: auto;
    }

    /* The Modal (background) */
    .modal {
        display: none; /* Hidden by default */
        position: fixed; /* Stay in place */
        z-index: 1; /* Sit on top */
        padding-top: 100px; /* Location of the box */
        left: 0;
        top: 0;
        width: 100%; /* Full width */
        height: 100%; /* Full height */
        overflow: auto; /* Enable scroll if needed */
        background-color: rgb(0, 0, 0); /* Fallback color */
        background-color: rgba(0, 0, 0, 0.4); /* Black w/ opacity */
    }

    /* Modal Content */
    .modal-content {
        background-color: #0d3648;
        margin: auto;
        padding: 20px;
        border: 1px solid #888;
        width: 50%;
        color: #ffffff;
        font-family: Roboto;
    }

    select {
        width: 100%;
        height: 100%;
        border: none;
        background: none;
        text-align-last: center;
        cursor: pointer;
    }

    .modal-content p {
        font-size: 26px;
    }

    .modal-content .modal-item {
        margin-top: 16px;
    }

    .modal-content .modal-item a {
        color: #ffffff;
        font-size: 20px;
    }

    .modal-content .modal-item a i {
        padding-right: 6px;
    }

    .modal-content #modal-errors {
        margin-top: 5px;
    }

    /* The Close Button */
    .close {
        color: #ffffff;
        float: right;
        font-size: 28px;
        font-weight: bold;
    }

    .close:hover,
    .close:focus {
        color: #000;
        text-decoration: none;
        cursor: pointer;
    }

    table {
        font-family: Roboto;
        border-collapse: separate;
        border-spacing: 2px;
        width: 70%;
        overflow: scroll;
        position: relative;
        left: 25%;
    }

    .content {
        display: none;
    }

    #clients {
        margin-bottom: 40px;
        display: none;
    }

    .new-clients {
        display: none;
    }

    .new-users {
        margin-top: 50px;
    }

    td {
        margin:0;padding:0;
        text-align: center;
        padding: 8px;
        background-color: #f0f0f0;
    }

    th {
        margin:0;padding:0;
        text-align: center;
        padding: 8px;
        background-color: #0d3648;
        color: #ffffff
    }
    table-title {
        padding-bottom: 10px;
        margin: 0 0 10px;
        font-size: 40px;
    }

    table a.update, table a.new-user, table a.update, table a.new-client {
        color: #1c7430;
    }

    table a.update {
        color: #ffc107;
    }

    table a.delete {
        color: #a60000;
    }

    table input {
        width: 100%;
        height: 100%;
        background: inherit;
        border: none;
        font-size: 16px;
        text-align: center;
    }

    .search {
        width: 30%;
        position: relative;
        display: flex;
        left: 25%;
        margin-top: 50px;
        margin-bottom: 40px;
        font-size: medium;
        font-family: Roboto;
    }

    .searchTerm {
        width: 50%;
        border: 3px solid #0d3648;
        border-right: none;
        padding: 5px;
        height: 36px;
        border-radius: 5px 0 0 5px;
        outline: none;
        color: #000000;
    }

    .searchTerm:focus{
        color: #000000;
    }
    .searchButton {
        width: 40px;
        height: 36px;
        border: 1px solid #0d3648;
        background: #0d3648;
        text-align: center;
        color: #fff;
        border-radius: 0 5px 5px 0;
        cursor: pointer;
        font-size: 20px;
    }

    </style>
    <script src="https://kit.fontawesome.com/b99e675b6e.js"></script>
</head>

<body>

<!-- The Modal -->
<div id="myModal" class="modal">

    <!-- Modal content -->
    <div class="modal-content">
        <span class="close">&times;</span>

        <p id="modal-title"></p>

        <ul id="modal-errors">
        </ul>
    </div>
</div>

<div class="wrapper">
    <div class="sidebar">
        <h2>Patronus</h2>
        <h2 style="margin-bottom: 30px; font-size: medium">¡Hola, ${user.name}!</h2>
        <ul>
            <li id="operators-section"><a class="sidebar-action" id="operators-id" href="#empleados"> Operadores</a>
            </li>
            <li id="clients-section"><a class="sidebar-action" id="clients-id" href="#clientes">Clientes</a></li>
            <li><a id="alert-id" href="/liveAlerts/index?user_id=${user.id}&company=${user.company}&user_type=admin">Alertas</a></li>
            <li>
                <a href="/logoff">Salir</a>
            </li>
        </ul>
    </div>
</div>

<div class="content crud-employee" id="crud-employee" style="margin-top: 2%">
<table>
    <tr>
        <th style="font-size: 26px; background-color: #FFFFFF; color: #000000; text-align: left; padding-left: 0; font-weight: normal">Detalle <b>Operadores</b></th>
    </tr>
</table>
    <table id="employees">
        <tr>
            <th>Legajo</th>
            <th>Nombre</th>
            <th>Apellido</th>
            <th>Mail</th>
            <th>Acciones</th>
        </tr>
        <g:each in="${employees}" var="user">
            <tr id="employee_row_${user.legajo}">
                <td>${user.legajo}</td>
                <td><input type="text" id="employee_first_name_${user.legajo}" value="${user.first_name}"></td>
                <td><input type="text" id="employee_last_name_${user.legajo}" value="${user.last_name}"></td>
                <td><input type="text" id="employee_mail_${user.legajo}" value="${user.mail}"></td>
                <td><a class="update" attr-company-id="${user.cuit}" attr-user-id="${user.legajo}" href="#" style="width: 22px">
                    <i class="fas fa-edit"></i>
                </a>
                <a class="delete" attr-company-id="${user.cuit}" attr-user-id="${user.legajo}" href="#">
                    <i class="fas fa-trash"></i>
                </a></td>
            </tr>
        </g:each>
    </table>
    <table style="margin-top: 2%">
        <tr>
            <th style="font-size: 26px; background-color: #FFFFFF; color: #000000; text-align: left; padding-left: 0; font-weight: normal">Agregar <b>Operador</b></th>
        </tr>
    </table>
    <table class="new-users" style="margin-top: 0">
        <tr>
            <th>Legajo</th>
            <th>Nombre</th>
            <th>Apellido</th>
            <th>Mail</th>
            <th>Contraseña</th>
            <th>Acciones</th>
        </tr>
        <tr id="row_new">
            <td><input type="text" id="employee_legajo_new" placeholder="Ingresar Legajo"></td>
            <td><input type="text" id="employee_first_name_new" placeholder="Ingresar Nombre"></td>
            <td><input type="text" id="employee_last_name_new" placeholder="Ingresar Apellido"></td>
            <td><input type="text" id="employee_mail_new" placeholder="Ingresar Mail"></td>
            <td><input type="password" id="employee_password_new" placeholder="Ingresar Contraseña"></td>
            <td><a class="new-user" attr-company-id="${company}" href="#">
                <div>
                    <i class="fas fa-check-circle"></i>
                </div>
            </a></td>
        </tr>
    </table>

</div>

<div class="content crud-clients" id="crud-clients">
        <div class="search">
            <input type="text" class="searchTerm" id="client_search" placeholder="Nro. Afiliado">
            <button type="submit" class="searchButton" id="client_search_submit" input attr-company-id="${company}">
                <i class="fa fa-search"></i>
            </button>
        </div>
    <table id="clients-header" style="margin-top: 4%; visibility: hidden">
        <tr>
            <th style="font-size: 26px; background-color: #FFFFFF; color: #000000; text-align: left; padding-left: 0; font-weight: normal">Grupo Familiar</th>
        </tr>
    </table>
    <table id="clients">
        <tr>
            <th>Tipo Documento</th>
            <th>Número Documento</th>
            <th>Nombre y Apellido</th>
            <th>Celular</th>
            <th>Acciones</th>
        </tr>
    </table>
    <table id="new-clients-header" style="margin-top: 4%; visibility: hidden">
        <tr>
            <th style="font-size: 26px; background-color: #FFFFFF; color: #000000; text-align: left; padding-left: 0; font-weight: normal">Habilitar <b>DNI</b></th>
        </tr>
    </table>
    <table class="new-clients" id="new-clients">
        <tr>
            <th>Tipo Documento</th>
            <th>Número Documento</th>
            <th>Acciones</th>
        </tr>
        <tr id="row_new_client">
            <td>
                <select id="client_tipo_documento_new">
                    <option value="DNI">DNI</option>
                    <option value="LE">LE</option>
                    <option value="LC">LC</option>
                </select>
            </td>
            <td><input type="text" id="client_numero_documento_new" placeholder="Ingresar Número Documento"></td>
            <td><a class="new-client" attr-company-id="${company}" href="#">
                <div>
                    <i class="fas fa-check-circle"></i>
                </div>
            </a></td>
        </tr>
    </table>

</div>

<script>
    showContent();

    function showContent() {
        const crudClients = document.getElementById("crud-clients");
        crudClients.style.display = "none";

        const crudEmployee = document.getElementById("crud-employee");
        crudEmployee.style.display = "none";

        const clientSection = document.getElementById("clients-section");
        clientSection.className = "";

        const employeeSection = document.getElementById("operators-section");
        employeeSection.className = "";


        switch (document.location.hash) {
            case "#clientes":
                crudClients.style.display = "block";
                clientSection.className = "active";
                break;
            default:
                crudEmployee.style.display = "block";
                employeeSection.className = "active";
        }
    }

    document.querySelectorAll(".crud-employee .delete")
        .forEach(link => deleteEmployeeListener(link));

    document.querySelectorAll(".crud-employee .update")
        .forEach(link => updateEmployeeListener(link));

    document.querySelectorAll(".new-user")
        .forEach(link => createNewEmployeeListener(link));

    document.querySelectorAll(".new-client")
        .forEach(link => createNewClientListener(link));

    document.querySelector(".modal-content span.close").addEventListener('click', (e) => {
        const modal = document.getElementById("myModal");

        modal.style.display = "none";
    });

    const searchButton = document.getElementById("client_search_submit");
    searchButton.addEventListener("click", (e) => {
        e.preventDefault();
        const clientId = document.getElementById("client_search").value;
        const company = searchButton.getAttribute("attr-company-id")

        const request = new XMLHttpRequest()
        const url = "/company/" + company +
            "/clients/" + clientId;

        request.open('GET', url);
        request.send();

        request.onreadystatechange = (e) => {
            if (request.readyState !== XMLHttpRequest.DONE) {
                return;
            }

            let response = JSON.parse(request.responseText);

            const rows = Array.prototype.slice.call(document.querySelectorAll('#clients tr'), 1);

            rows.forEach(row => row.remove());

            response.users.forEach(user => {
                createRowClients(company, clientId, user.document_type.toUpperCase(), user.document,
                                    user.first_name, user.last_name, user.area_code, user.phone_number);
            });

            document.getElementById("new-clients-header").style.visibility = "visible";
            document.getElementById("new-clients").style.display = "table";
            document.getElementById("clients-header").style.visibility = "visible";
            document.getElementById("clients").style.display = "table";

            document.getElementById("row_new_client").setAttribute("attr-client-id", clientId);
        };
    });

    document.querySelectorAll(".sidebar-action").forEach(action =>
        action.addEventListener("click", () => {
            setTimeout(showContent, 0);
        })
    );

    function updateEmployeeListener(link) {
        link.addEventListener('click', (e) => {
            e.preventDefault();
            let icon = link.querySelector("i.fas");
            let originalClass = icon.className;
            icon.className = "fas fa-spinner fa-spin";

            const userId = link.getAttribute("attr-user-id");

            const request = new XMLHttpRequest()
            const url = "/company/" + link.getAttribute("attr-company-id") +
                "/user/" + userId + "/update";

            const firstName = document.getElementById("employee_first_name_" + userId).value;
            const lastName = document.getElementById("employee_last_name_" + userId).value;
            const mail = document.getElementById("employee_mail_" + userId).value;

            request.open('POST', url);
            request.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
            request.send(JSON.stringify({
                first_name: firstName,
                last_name: lastName,
                mail: mail
            }));

            request.onreadystatechange = (e) => {
                if (request.readyState !== XMLHttpRequest.DONE) {
                    return;
                }

                icon.className = originalClass;

                if (request.status >= 400) {
                    const modal = document.getElementById("myModal");
                    const response = JSON.parse(request.responseText);

                    document.getElementById("modal-title").textContent = "Error actualizando operador";
                    let ul = document.getElementById("modal-errors");

                    ul.innerHTML = '';

                    response.errors.forEach(err => {
                        const li = document.createElement("li");

                        li.appendChild(document.createTextNode(err));

                        ul.appendChild(li);
                    });

                    modal.style.display = "block";
                }else{
                    const modal = document.getElementById("myModal");

                    document.getElementById("modal-title").textContent = "Operador Actualizado";
                    let ul = document.getElementById("modal-errors");

                    ul.innerHTML = '';

                    const li = document.createElement("li");

                    li.appendChild(document.createTextNode("Los cambios fueron aplicados correctamente."));

                    ul.appendChild(li);

                    modal.style.display = "block";

                    setTimeout(function(){
                        modal.style.display = "none";
                    }, 1000);
                }

                console.log(request.responseText);
            }
        });
    }

    function createNewClientListener(link) {
        link.addEventListener('click', (e) => {
            e.preventDefault();
            let icon = link.querySelector("i.fas");
            let originalClass = icon.className;
            icon.className = "fas fa-spinner fa-spin";

            const userId = document.getElementById("row_new_client").getAttribute("attr-client-id");
            const company = link.getAttribute("attr-company-id");

            const request = new XMLHttpRequest()
            const url = "/company/" + company +
                "/clients/" + userId + "/new";

            const documentNumber = document.getElementById("client_numero_documento_new").value;
            const documentType = document.getElementById("client_tipo_documento_new").value;

            request.open('POST', url);
            request.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
            request.send(JSON.stringify({
                document: documentNumber,
                document_type: documentType
            }));

            request.onreadystatechange = (e) => {
                if (request.readyState !== XMLHttpRequest.DONE) {
                    return;
                }

                icon.className = originalClass;
                const response = JSON.parse(request.responseText);

                if (request.status >= 200 && request.status < 300) {
                    const tableRows = document.querySelectorAll("#clients tr");

                    if (response.users.length === tableRows.length - 1) {
                        const modal = document.getElementById("myModal");

                        document.getElementById("modal-title").textContent = "Error habilitando cliente";
                        let ul = document.getElementById("modal-errors");

                        ul.innerHTML = '';

                        const li = document.createElement("li");

                        li.appendChild(document.createTextNode("Documento ya habilitado"));

                        ul.appendChild(li);

                        modal.style.display = "block";
                    } else {
                        createRowClients(company, userId, documentType, documentNumber);

                        document.getElementById("client_numero_documento_new").value = '';
                        document.getElementById("client_tipo_documento_new").value = 'DNI';

                        const modal = document.getElementById("myModal");

                        document.getElementById("modal-title").textContent = "Cliente Habilitado";
                        let ul = document.getElementById("modal-errors");

                        ul.innerHTML = '';

                        const li = document.createElement("li");

                        li.appendChild(document.createTextNode("El documento ingresado ha sido habilitado en la aplicación."));

                        ul.appendChild(li);

                        modal.style.display = "block";

                        setTimeout(function(){
                            modal.style.display = "none";
                        }, 1000);
                    }
                } else {
                    const modal = document.getElementById("myModal");

                    document.getElementById("modal-title").textContent = "Error habilitando cliente";
                    let ul = document.getElementById("modal-errors");

                    ul.innerHTML = '';

                    response.errors.forEach(err => {
                        const li = document.createElement("li");

                        li.appendChild(document.createTextNode(err));

                        ul.appendChild(li);
                    });

                    modal.style.display = "block";
                }

                console.log(request.responseText);
            }
        });
    }

    function createNewEmployeeListener(link) {
        link.addEventListener('click', (e) => {
            e.preventDefault();
            let icon = link.querySelector("i.fas");
            let originalClass = icon.className;
            icon.className = "fas fa-spinner fa-spin";

            const userId = document.getElementById("employee_legajo_new").value;
            const company = link.getAttribute("attr-company-id");

            const request = new XMLHttpRequest()
            const url = "/company/" + company +
                "/user/" + userId + "/new";

            const firstName = document.getElementById("employee_first_name_new").value;
            const lastName = document.getElementById("employee_last_name_new").value;
            const mail = document.getElementById("employee_mail_new").value;
            const password = document.getElementById("employee_password_new").value;

            request.open('POST', url);
            request.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
            request.send(JSON.stringify({
                legajo: userId,
                first_name: firstName,
                last_name: lastName,
                mail: mail,
                password: password
            }));

            request.onreadystatechange = (e) => {
                if (request.readyState !== XMLHttpRequest.DONE) {
                    return;
                }

                icon.className = originalClass;

                if (request.status === 201) {
                    createRowEmployee(company, userId, firstName, lastName, mail);

                    document.getElementById("employee_legajo_new").value = '';
                    document.getElementById("employee_first_name_new").value = '';
                    document.getElementById("employee_last_name_new").value = '';
                    document.getElementById("employee_mail_new").value = '';
                    document.getElementById("employee_password_new").value = '';

                    const modal = document.getElementById("myModal");

                    document.getElementById("modal-title").textContent = "Operador Creado";
                    let ul = document.getElementById("modal-errors");

                    ul.innerHTML = '';

                    const li = document.createElement("li");

                    li.appendChild(document.createTextNode("El nuevo operador ha sido dado de alta satisfactoriamente."));

                    ul.appendChild(li);

                    modal.style.display = "block";

                    setTimeout(function(){
                        modal.style.display = "none";
                    }, 1000);
                } else {
                    const modal = document.getElementById("myModal");
                    const response = JSON.parse(request.responseText);

                    document.getElementById("modal-title").textContent = "Error creando operador";
                    let ul = document.getElementById("modal-errors");

                    ul.innerHTML = '';

                    response.errors.forEach(err => {
                        const li = document.createElement("li");

                        li.appendChild(document.createTextNode(err));

                        ul.appendChild(li);
                    });

                    modal.style.display = "block";
                }

                console.log(request.responseText);
            }
        });
    }

    function deleteEmployeeListener(link) {
        link.addEventListener('click', (e) => {
            e.preventDefault();
            let icon = link.querySelector("i.fas");
            let originalClass = icon.className;
            icon.className = "fas fa-spinner fa-spin";

            const userId = link.getAttribute("attr-user-id");

            const request = new XMLHttpRequest()
            const url = "/company/" + link.getAttribute("attr-company-id") +
                "/user/" + userId + "/delete";

            request.open('DELETE', url);
            request.send();

            request.onreadystatechange = (e) => {
                if (request.readyState !== XMLHttpRequest.DONE) {
                    return;
                }

                console.log(request.responseText);

                icon.className = originalClass;

                if (request.status === 200 || request.status === 204) {
                    document.getElementById("employee_row_" + userId).remove();

                    const modal = document.getElementById("myModal");

                    document.getElementById("modal-title").textContent = "Operador Eliminado";
                    let ul = document.getElementById("modal-errors");

                    ul.innerHTML = '';

                    const li = document.createElement("li");

                    li.appendChild(document.createTextNode("El operador ha sido eliminado satisfactoriamente."));

                    ul.appendChild(li);

                    modal.style.display = "block";

                    setTimeout(function(){
                        modal.style.display = "none";
                    }, 1000);
                } else {
                    const modal = document.getElementById("myModal");
                    const response = JSON.parse(request.responseText);

                    document.getElementById("modal-title").textContent = "Error borrando operador";
                    let ul = document.getElementById("modal-errors");

                    ul.innerHTML = '';

                    response.errors.forEach(err => {
                        const li = document.createElement("li");

                        li.appendChild(document.createTextNode(err));

                        ul.appendChild(li);
                    });

                    modal.style.display = "block";
                }


                console.log(request.responseText);
            }
        });
    }

    function deleteClientListener(link) {
        link.addEventListener('click', (e) => {
            e.preventDefault();

            let icon = link.querySelector("i.fas");
            let originalClass = icon.className;
            icon.className = "fas fa-spinner fa-spin";

            const userId = link.getAttribute("attr-user-id");
            const documentId = link.getAttribute("attr-document-number");

            const row = document.getElementById("client_row_" + documentId);

            const request = new XMLHttpRequest()
            const url = "/company/" + link.getAttribute("attr-company-id") +
                "/clients/" + userId + "/delete";

            request.open('POST', url);
            request.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
            request.send(JSON.stringify({
                document: row.getAttribute("attr-document-number"),
                document_type: row.getAttribute("attr-document-type"),
            }));

            request.onreadystatechange = (e) => {
                if (request.readyState !== XMLHttpRequest.DONE) {
                    return;
                }

                console.log(request.responseText);
                icon.className = originalClass;

                if (request.status === 200 || request.status === 204) {
                    document.getElementById("client_row_" + documentId).remove();

                    const modal = document.getElementById("myModal");

                    document.getElementById("modal-title").textContent = "Cliente Eliminado";
                    let ul = document.getElementById("modal-errors");

                    ul.innerHTML = '';

                    const li = document.createElement("li");

                    li.appendChild(document.createTextNode("El documento ha sido dado de baja para utilizar la aplicación."));

                    ul.appendChild(li);

                    modal.style.display = "block";

                    setTimeout(function(){
                        modal.style.display = "none";
                    }, 1000);
                } else {
                    const modal = document.getElementById("myModal");
                    const response = JSON.parse(request.responseText);

                    document.getElementById("modal-title").textContent = "Error borrando cliente";
                    let ul = document.getElementById("modal-errors");

                    ul.innerHTML = '';

                    response.errors.forEach(err => {
                        const li = document.createElement("li");

                        li.appendChild(document.createTextNode(err));

                        ul.appendChild(li);
                    });

                    modal.style.display = "block";
                }
            }
        });
    }

    function createRowEmployee(company, userId, firstName, lastName, mail) {
        const tableRef = document.getElementById('employees').getElementsByTagName('tbody')[0];

        const newRow = tableRef.insertRow();
        newRow.id = "employee_row_" + userId;

        const cellLegajo = newRow.insertCell(0);
        const textLegajo = document.createTextNode(userId);

        cellLegajo.appendChild(textLegajo);

        const cellFirstName = newRow.insertCell(1);
        cellFirstName.appendChild(createField("employee_first_name", userId, firstName));

        const cellLastName = newRow.insertCell(2);
        cellLastName.appendChild(createField("employee_last_name", userId, lastName));

        const cellMail = newRow.insertCell(3);
        cellMail.appendChild(createField("employee_mail", userId, mail));

        const cellActions = newRow.insertCell(4);
        createIcon(company, userId, cellActions);
    }

    function createRowClients(company, clientId, documentType, documentNumber, firstName, lastName, areaCode, phoneNumber) {
        const tableRef = document.getElementById('clients').getElementsByTagName('tbody')[0];

        const newRow = tableRef.insertRow();
        newRow.id = "client_row_" + documentNumber;
        newRow.setAttribute("attr-document-type", documentType);
        newRow.setAttribute("attr-document-number", documentNumber);
        newRow.setAttribute("attr-firstLastName", (firstName + ' ' + lastName).toUpperCase());
        newRow.setAttribute("attr-phone", areaCode + ' ' + phoneNumber);

        const cellDocumentType = newRow.insertCell(0);
        const textDocumentType = document.createTextNode(documentType);

        cellDocumentType.appendChild(textDocumentType);

        const cellDocumentNumber = newRow.insertCell(1);
        const textDocumentNumber = document.createTextNode(documentNumber);

        cellDocumentNumber.appendChild(textDocumentNumber);

        const cellfirstLastName = newRow.insertCell(2);
        var textfirstLastName;

        if(firstName && lastName) {
            textfirstLastName = document.createTextNode((firstName + ' ' + lastName).toUpperCase());
        } else {
            textfirstLastName = document.createTextNode('-');
        }
        cellfirstLastName.appendChild(textfirstLastName);

        const cellPhone = newRow.insertCell(3);
        var textPhone;

        if(areaCode && phoneNumber) {
            textPhone = document.createTextNode(areaCode + ' ' + phoneNumber);
        } else {
            textPhone = document.createTextNode('-');
        }
        cellPhone.appendChild(textPhone);

        const cellActions = newRow.insertCell(4);
        const del = deleteAction(company, clientId, documentNumber, documentType);

        deleteClientListener(del);
        cellActions.appendChild(del);
    }

    function deleteAction(company, userId, documentNumber, documentType) {
        const del = document.createElement("a");
        del.className = 'delete';
        del.setAttribute('attr-company-id', company);
        del.setAttribute('attr-document-number', documentNumber);
        del.setAttribute('attr-document-type', documentType);
        del.setAttribute('attr-user-id', userId);
        del.href = "#";

        const ic = document.createElement("i");
        ic.className = "fas fa-trash";

        del.appendChild(ic);

        return del
    }

    function createIcon(company, userId, cellActions) {
        const del = document.createElement("a");
        del.className = 'delete';
        del.setAttribute('attr-company-id', company);
        del.setAttribute('attr-user-id', userId);
        del.href = "#";
        deleteEmployeeListener(del);

        const a = document.createElement("a");
        a.className = 'update';
        a.setAttribute('attr-company-id', company);
        a.setAttribute('attr-user-id', userId);
        a.href = "#";
        a.style.marginRight = "4.47px";
        updateEmployeeListener(a);

        const i = document.createElement("i");
        i.className = "fas fa-edit";

        const ic = document.createElement("i");
        ic.className = "fas fa-trash";

        a.appendChild(i);
        del.appendChild(ic);

        cellActions.appendChild(a);
        cellActions.appendChild(del);
    }

    function createField(field, legajo, value) {
        const input = document.createElement('input');

        input.type = "text"
        input.id = field + "_" + legajo;
        input.value = value;

        return input;
    }
</script>