<asset:stylesheet src="main_operador.css"/>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Patronus</title>
    <link rel="icon" type="image/x-ico" href="/assets/favicon.ico">
    <meta name="viewport" content="initial-scale=1,maximum-scale=1,user-scalable=no"/>
    <script src="https://api.mapbox.com/mapbox-gl-js/v1.12.0/mapbox-gl.js"></script>
    <link href="https://api.mapbox.com/mapbox-gl-js/v1.12.0/mapbox-gl.css" rel="stylesheet"/>
    <style>
    body {
        margin: 0;
        padding: 0;
    }

    #map {
        position: absolute;
        top: 0;
        bottom: 0;
        left: 60%;
        width: 40%;
    }

    #alertInfo {
        position: absolute;
        top: 0;
        bottom: 0;
        left: 20%;
        width: 40%;
    }

    .recording {
        width: 70%;
        height: auto;
        max-height: 450px;
    }

    /* The Modal (background) */
    .modal {
        display: none; /* Hidden by default */
        position: fixed; /* Stay in place */
        z-index: 1; /* Sit on top */
        padding-top: 100px; /* Location of the box */
        left: 0;
        top: 0;
        width: 100%; /* Full width */
        height: 100%; /* Full height */
        overflow: auto; /* Enable scroll if needed */
        background-color: rgb(0, 0, 0); /* Fallback color */
        background-color: rgba(0, 0, 0, 0.4); /* Black w/ opacity */
    }

    #search-event {
        display: inline;
    }

    #fetch_event {
        font-size: 20px;
        padding-left: 10px;
    }

    .event-searchbox {
        text-align: center;
    }

    /* Modal Content */
    .modal-content {
        background-color: #0d3648;
        margin: auto;
        padding: 20px;
        border: 1px solid #888;
        width: 50%;
        color: #ffffff;
    }

    .modal-content p {
        font-size: x-large;
    }

    .modal-content .modal-item {
        margin-top: 16px;
    }

    .modal-content .modal-item a {
        color: #ffffff;
        font-size: large;
    }

    .modal-content .modal-item a i {
        padding-right: 6px;
    }

    /* The Close Button */
    .close {
        color: #ffffff;
        float: right;
        font-size: 28px;
        font-weight: bold;
    }

    .close:hover,
    .close:focus {
        color: #000;
        text-decoration: none;
        cursor: pointer;
    }

    .search {
        width: 30%;
        position: relative;
        display: flex;
        font-size: medium;
        font-family: Roboto;
    }

    .searchTerm {
        width: 70%;
        border: 3px solid #FFFFFF;
        border-right: none;
        padding: 5px;
        height: 36px;
        border-radius: 5px 0 0 5px;
        outline: none;
        color: #000000;
    }

    .searchTerm:focus {
        color: #0d3648;
    }

    .searchButton {
        width: 40px;
        height: 36px;
        border-right: 1px solid #FFFFFF;
        border-top: 1px solid #FFFFFF;
        border-bottom: 1px solid #FFFFFF;
        border-left: 1px solid #0d3648;
        background: #FFFFFF;
        text-align: center;
        color: #0d3648;
        border-radius: 0 5px 5px 0;
        cursor: pointer;
        font-size: 20px;
    }

    </style>
    <script src="https://kit.fontawesome.com/b99e675b6e.js"></script>
</head>

<body>

<!-- The Modal -->
<div id="myModal" class="modal">

    <!-- Modal content -->
    <div class="modal-content">
        <span class="close">&times;</span>

        <p>¿Desea recibir una nueva alerta?</p>

        <ul>
            <li class="modal-item"><a class="accept-assign" href="#" attr-user-id="${user.id}"
                                      attr-company="${user.company}"><i
                        class="fas fa-check-circle"></i>Si, asignar nueva alerta.</a></li>

            <li class="modal-item"><a class="decline-assign" href="/logoff"><i
                    class="fas fa-times-circle"></i>No asignar nueva alerta.</a></li>
        </ul>
    </div>
</div>

<!-- The Modal -->
<div id="myModalError" class="modal">

    <!-- Modal content -->
    <div class="modal-content">
        <span class="close">&times;</span>

        <p id="modal-title-error"></p>

        <ul id="modal-error-errors">
        </ul>
    </div>
</div>

<g:set var="recording_url"
       value="${event.recording_id ? "https://fierce-caverns-84695.herokuapp.com/users/${event.client_id}/events/${event.id}/recordings/${event.recording_id}" : ""}"/>

<div class="wrapper">
    <div class="sidebar">
        <h2>Patronus</h2>

        <h2 style="margin-bottom: 30px; font-size: medium">¡Hola, ${user.name}!</h2>
        <ul>
            <g:if test="${user.type == "admin"}">
                <li><a href="/admin/index?user_id=${user.id}&company=${user.company}#empleados">Operadores</a>
                </li>
                <li><a href="/admin/index?user_id=${user.id}&company=${user.company}#clientes">Clientes</a></li>
            </g:if>
            <li class="active"><a id="alert-id" href="#">Alertas</a></li>
            <li>
                <a href="/logoff">Salir</a>
            </li>
        </ul>
    </div>
</div>

<div id="alertInfo" class="sidebar-alert">

    <g:if test="${user.type == "employee"}">
        <h2>Información de la Alerta</h2>
        <g:if test="${event.id == ''}">
            <ul>
                <li id="alert-not-found">
                    <a style="font-weight: bold">No hay alertas disponibles.</a>
                </li>
                <li id="alert-data-id" style="visibility: hidden">
                    <a id="alertaid">Alerta ID: ${event.id}</a>
                    <a id="eventDate">Fecha y hora: ${event.date}</a>
                    <br>
                    <a id="clienteID">Cliente ID: ${event.client_id}</a>
                    <a id="nroCliente">Nro. Cliente: ${client.client_number}</a>
                    <a id="nomYape">Nombre y Apellido: ${client.first_name + " " + client.last_name}</a>
                    <a id="celular">Celular: ${client.area_code + " " + client.phone_number}</a>
                </li>
                <li id="video-alert" style="visibility: hidden"><center>
                    <video class="recording" controls>
                        <source src="${recording_url}"
                                type="video/mp4">
                    </video>
                </center>
                </li>
                <li id="end-event-id" style="visibility: hidden">
                    <a class="end-event" href="#" attr-event-id="${event.id}" attr-client-id="${event.client_id}"><i
                            class="fas fa-check-circle"></i>Finalizar Alerta</a>
                </li>

                <li id="free-event-id" style="visibility: hidden">
                    <a class="free-event" href="#" attr-event-id="${event.id}" attr-client-id="${event.client_id}"><i
                            class="fas fa-minus-circle"></i>Liberar Alerta</a>
                </li>
            </ul>
        </g:if>
        <g:if test="${event.id != ''}">
            <ul>
                <li style="visibility: hidden" id="alert-not-found">
                    <a style="font-weight: bold">No hay alertas disponibles.</a>
                </li>
                <li id="alert-data-id">
                    <a id="alertaid">Alerta ID: ${event.id}</a>
                    <a id="eventDate">Fecha y hora: ${event.date}</a>
                    <br>
                    <a id="clienteID">Cliente ID: ${event.client_id}</a>
                    <a id="nroCliente">Nro. Cliente: ${client.client_number}</a>
                    <a id="nomYape">Nombre y Apellido: ${client.first_name + " " + client.last_name}</a>
                    <a id="celular">Celular: ${client.area_code + " " + client.phone_number}</a>
                </li>
                <li id="video-alert"><center>
                    <video class="recording" controls>
                        <source src="${recording_url}"
                                type="video/mp4">
                    </video>
                </center>
                </li>
                <li id="end-event-id">
                    <a class="end-event" href="#" attr-event-id="${event.id}" attr-client-id="${event.client_id}"><i
                            class="fas fa-check-circle"></i>Finalizar Alerta</a>
                </li>

                <li id="free-event-id">
                    <a class="free-event" href="#" attr-event-id="${event.id}" attr-client-id="${event.client_id}"><i
                            class="fas fa-minus-circle"></i>Liberar Alerta</a>
                </li>
            </ul>
        </g:if>
    </g:if>
    <g:if test="${user.type == "admin"}">
        <h2>Visualización de Alertas</h2>
        <ul>
            <li><a id="assign-event-id" class="accept-assign" href="#" attr-user-id="${user.id}"
                   attr-company="${user.company}"><i
                        class="fas fa-shield-alt"></i>Asignar Alerta</a>
            </li>
            <li><center>
                <div class="search">
                    <input type="text" class="searchTerm" id="fetch_event" placeholder="Alerta ID"
                           style="font-family: Roboto; font-size: medium">
                    <button type="submit" class="searchButton" id="search-event" href="#" attr-user-id="${user.id}"
                            attr-company="${user.company}">
                        <i class="fa fa-search"></i>
                    </button>
                    <br>
                </div>

                <div style="height: 34px; visibility: hidden">
                    <a id="alarm-state" style="font-size: medium;padding-top: 15px; font-weight: bold"></a>
                </div>
            </center></li>

            <li id="alert-data-id" style="visibility: hidden">
                <a id="alertaid"></a>
                <a id="eventDate"></a>
                <br>
                <a id="clienteID"></a>
                <a id="nroCliente"></a>
                <a id="nomYape"</a>
                <a id="celular"></a>
            </li>
            <li id="video-alert" style="visibility: hidden"><center>
                <video class="recording" controls>
                    <source src="${recording_url}"
                            type="video/mp4">
                </video>
            </center>
            </li>
            <li id="end-event-id" style="visibility: hidden">
                <a class="end-event" href="#" attr-event-id="${event.id}" attr-client-id="${event.client_id}"><i
                        class="fas fa-check-circle"></i>Finalizar Alerta</a>
            </li>

            <li id="free-event-id" style="visibility: hidden">
                <a class="free-event" href="#" attr-event-id="${event.id}" attr-client-id="${event.client_id}"><i
                        class="fas fa-minus-circle"></i>Liberar Alerta</a>
            </li>
        </ul>
    </g:if>
</ul>
</div>

<div id="map" style="visibility: hidden"></div>

<script>

    const userType = '${user.type}';
    let map = null;
    let localizations = JSON.parse('${event.localizations}'.replaceAll('&quot;', '"'));

    showMap(JSON.parse('${event.localizations}'.replaceAll('&quot;', '"')))

    addCallbacks();

    setInterval(fetchEvent, 15000);
    setInterval(updateLocalizations, 15000);

    function addCallbacks() {
        const endEventsButtons = document.querySelectorAll(".end-event")

        endEventsButtons.forEach(button => {
            // button.removeEventListener('click')
            button.addEventListener('click', () => {

                let icon = button.querySelector("i.fas");
                let originalClass = icon.className;
                icon.className = "fas fa-spinner fa-spin";

                const request = new XMLHttpRequest()
                const url = "/users/" + button.getAttribute("attr-client-id") +
                    "/events/" + button.getAttribute("attr-event-id") + "/close";

                request.open('POST', url);
                request.send();

                request.onreadystatechange = (e) => {
                    const modal = document.getElementById("myModal");
                    modal.style.display = "block";
                    icon.className = originalClass;

                    console.log(request.responseText)
                }
            });
        });

        const freeEventButtons = document.querySelectorAll(".free-event")

        freeEventButtons.forEach(button => {
            // button.removeEventListener('click')
            button.addEventListener('click', () => {

                let icon = button.querySelector("i.fas");
                let originalClass = icon.className;
                icon.className = "fas fa-spinner fa-spin";

                const request = new XMLHttpRequest()
                const url = "/users/" + button.getAttribute("attr-client-id") +
                    "/events/" + button.getAttribute("attr-event-id") + "/free";

                request.open('POST', url);
                request.send();

                request.onreadystatechange = (e) => {
                    const modal = document.getElementById("myModal");
                    modal.style.display = "block";


                    icon.className = originalClass

                    console.log(request.responseText)
                }
            });
        });
        const assignButtons = document.querySelectorAll(".accept-assign");

        assignButtons.forEach(assignButton => {
            assignButton.addEventListener('click', () => {
                let icon = assignButton.querySelector("i.fas");
                let originalClass = icon.className;
                icon.className = "fas fa-spinner fa-spin";

                assignEvent(assignButton, (assigned) => {
                    icon.className = originalClass;
                    const modal = document.getElementById("myModal");
                    modal.style.display = "none";

                    if (!assigned) {
                        const modal = document.getElementById("myModalError");

                        document.getElementById("modal-title-error").textContent = "Error de asignación de alerta";
                        let ul = document.getElementById("modal-error-errors");

                        ul.innerHTML = '';

                        const li = document.createElement("li");

                        li.appendChild(document.createTextNode("No hay alertas disponibles."));

                        ul.appendChild(li);

                        const notFoundMessage = document.querySelector("#alert-not-found");

                        if(notFoundMessage){
                            notFoundMessage.style.visibility = "visible";
                        }

                        modal.style.display = "block";
                        document.getElementById("map").style.visibility = "hidden";
                        document.querySelector("#alert-data-id").style.visibility = "hidden";

                        document.getElementById("video-alert").style.visibility = "hidden";
                        document.getElementById("free-event-id").style.visibility = "hidden";
                        document.getElementById("end-event-id").style.visibility = "hidden";


                        const video = document.querySelector(".recording");

                        video.setAttribute('src', '');

                        document.querySelector(".end-event").setAttribute("attr-event-id", "");
                    }
                });
            });
        });

        const searchEventButton = document.getElementById("search-event");

        if (searchEventButton) {
            searchEventButton.addEventListener('click', () => {

                const request = new XMLHttpRequest()
                const url = "/company/" + searchEventButton.getAttribute("attr-company") +
                    "/users/" + searchEventButton.getAttribute("attr-user-id") + "/assign/" + document.getElementById("fetch_event").value;

                request.open('GET', url);
                request.send();

                const modal = document.getElementById("myModal");
                modal.style.display = "none";

                request.onreadystatechange = (e) => {
                    if (request.readyState !== XMLHttpRequest.DONE) {
                        return;
                    }

                    const event = JSON.parse(request.responseText);
                    console.log(event)

                    if (event.client_id) {
                        const alertMessage = document.querySelector("#alert-id");

                        // alertMessage.innerHTML = alertMessage.innerHTML.replace(/Alerta( \d*)?/, "Alerta " + event.id);

                        setTimeout(() => {
                            const video = document.querySelector(".recording");

                            video.setAttribute('src', "https://fierce-caverns-84695.herokuapp.com/users/" +
                                event.client_id + "/events/" + event.id + "/recordings/" + event.recording_id);
                            video.load();
                        }, 0);

                        const requestClient = new XMLHttpRequest()
                        const url = "/users/" + event.client_id;

                        requestClient.open('GET', url);
                        requestClient.send();

                        requestClient.onreadystatechange = (e) => {
                            if (requestClient.readyState !== XMLHttpRequest.DONE) {
                                return;
                            }

                            let alarmUser = JSON.parse(requestClient.responseText);
                            console.log(alarmUser);

                            document.getElementById("nomYape").text = "Nombre y Apellido: " + alarmUser.first_name + " " + alarmUser.last_name;
                            document.getElementById("celular").text = "Celular: " + alarmUser.area_code + " " + alarmUser.phone_number;
                            document.getElementById("nroCliente").text = "Nro. Cliente: " + alarmUser.client_number;
                        }

                        document.querySelector(".end-event").setAttribute("attr-event-id", event.id);
                        document.querySelector(".end-event").setAttribute("attr-client-id", event.client_id);

                        document.querySelector(".free-event").setAttribute("attr-event-id", event.id);
                        document.querySelector(".free-event").setAttribute("attr-client-id", event.client_id);

                        const alarmState = document.getElementById("alarm-state")

                        if (alarmState) {
                            alarmState.style.visibility = "hidden";
                        }
                        document.getElementById("video-alert").style.visibility = "visible";
                        document.getElementById("free-event-id").style.visibility = "visible";
                        document.getElementById("end-event-id").style.visibility = "visible";
                        document.getElementById("alert-data-id").style.visibility = "visible";

                        const notFoundMessage = document.querySelector("#alert-not-found");

                        if(notFoundMessage){
                            notFoundMessage.style.visibility = "hidden";
                        }

                        document.getElementById("alertaid").text = "Alerta ID: " + event.id;
                        document.getElementById("clienteID").text = "Usuario ID: " + event.client_id;

                        var d = new Date(event.date);
                        var formatedDate = formatDate(d);

                        document.getElementById("eventDate").text = "Fecha y hora: " + formatedDate;

                        showMap(event.localizations);
                    } else {
                        const alarmState = document.getElementById("alarm-state")

                        if (alarmState) {
                            alarmState.style.visibility = "visible";
                            alarmState.text = "* No existe una alerta asociada a ese ID.";
                        }

                        document.getElementById("video-alert").style.visibility = "hidden";
                        document.getElementById("free-event-id").style.visibility = "hidden";
                        document.getElementById("end-event-id").style.visibility = "hidden";
                        document.getElementById("alert-data-id").style.visibility = "hidden";
                        document.getElementById("map").style.visibility = "hidden";

                    }
                }
            });
        }

        document.querySelectorAll(".modal-content span.close").forEach(close => {
            close.addEventListener('click', (e) => {
                const modal = document.getElementById("myModal");

                modal.style.display = "none";

                const modalError = document.getElementById("myModalError");

                modalError.style.display = "none";
            });
        });
    }

    function formatDate(date) {
        var hours = date.getHours();
        var minutes = date.getMinutes();
        var seconds = date.getSeconds();
        minutes = minutes < 10 ? '0' + minutes : minutes;
        seconds = seconds < 10 ? '0' + seconds : seconds;
        var strTime = hours + ':' + minutes + ":" + seconds;
        return date.getDate() + "/" + (date.getMonth() + 1) + "/" + date.getFullYear() + "  " + strTime;
    }

    function showMap(localizations) {
        if (localizations.length === 0) {
            return;
        }

        document.getElementById("map").style.visibility = "visible";

        mapboxgl.accessToken = 'pk.eyJ1IjoibmF0aXRvIiwiYSI6ImNrZTY4bTBvNjFhZ2Yyc213N29kdXg2c2QifQ.42DIhYrm9VcNg2RP7rZCrw';
        map = new mapboxgl.Map({
            container: 'map', // container id
            style: 'mapbox://styles/mapbox/streets-v11', // style URL
            center: [localizations[localizations.length - 1].longitude, localizations[localizations.length - 1].latitude], // starting position [lng, lat]
            zoom: 15 // starting zoom
        });

        var size = 115;

        // implementation of CustomLayerInterface to draw a pulsing dot icon on the map
        // see https://docs.mapbox.com/mapbox-gl-js/api/#customlayerinterface for more info
        var pulsingDot = {
            width: size,
            height: size,
            data: new Uint8Array(size * size * 4),

// get rendering context for the map canvas when layer is added to the map
            onAdd: function () {
                var canvas = document.createElement('canvas');
                canvas.width = this.width;
                canvas.height = this.height;
                this.context = canvas.getContext('2d');
            },

// called once before every frame where the icon will be used
            render: function () {
                var duration = 1000;
                var t = (performance.now() % duration) / duration;

                var radius = (size / 2) * 0.3;
                var outerRadius = (size / 2) * 0.7 * t + radius;
                var context = this.context;

// draw outer circle
                context.clearRect(0, 0, this.width, this.height);
                context.beginPath();
                context.arc(
                    this.width / 2,
                    this.height / 2,
                    outerRadius,
                    0,
                    Math.PI * 2
                );
                context.fillStyle = 'rgba(255, 200, 200,' + (1 - t) + ')';
                context.fill();

// draw inner circle
                context.beginPath();
                context.arc(
                    this.width / 2,
                    this.height / 2,
                    radius,
                    0,
                    Math.PI * 2
                );
                context.fillStyle = 'rgba(255, 100, 100, 1)';
                context.strokeStyle = 'white';
                context.lineWidth = 2 + 4 * (1 - t);
                context.fill();
                context.stroke();

// update this image's data with data from the canvas
                this.data = context.getImageData(
                    0,
                    0,
                    this.width,
                    this.height
                ).data;

// continuously repaint the map, resulting in the smooth animation of the dot
                map.triggerRepaint();

// return `true` to let the map know that the image was updated
                return true;
            }
        };

        map.on('load', function () {

            var marker = new mapboxgl.Marker({"color": "#FF6464"})
                .setLngLat([localizations[0].longitude, localizations[0].latitude])
                .addTo(map);

            map.addImage('pulsing-dot', pulsingDot, {pixelRatio: 2});

            map.addSource('route', {
                'type': 'geojson',
                'data': {
                    'type': 'Feature',
                    'properties': {},
                    'geometry': {
                        'type': 'LineString',
                        'coordinates': localizations.map(l => [l.longitude, l.latitude])
                    }
                }
            });

            map.addLayer({
                'id': 'route',
                'type': 'line',
                'source': 'route',
                'layout': {
                    'line-join': 'round',
                    'line-cap': 'round'
                },
                'paint': {
                    'line-color': '#FF6464',
                    'line-width': 8
                }
            });

            map.addSource('points', {
                'type': 'geojson',
                'data': {
                    'type': 'FeatureCollection',
                    'features': [
                        {
                            'type': 'Feature',
                            'geometry': {
                                'type': 'Point',
                                'coordinates': [localizations[localizations.length - 1].longitude, localizations[localizations.length - 1].latitude]
                            }
                        }
                    ]
                }
            });
            map.addLayer({
                'id': 'points',
                'type': 'symbol',
                'source': 'points',
                'layout': {
                    'icon-image': 'pulsing-dot'
                }
            });
        });
    }

    function fetchEvent() {
        if (!document.querySelector(".end-event").getAttribute("attr-event-id") && userType !== "admin") {
            assignEvent(document.querySelector(".accept-assign"), () => {
            });
        }
    }

    function updateLocalizations() {
        let button = document.querySelector(".end-event");

        let eventId = button.getAttribute("attr-event-id")
        if (eventId) {
            const request = new XMLHttpRequest()
            const url = "/users/" + button.getAttribute("attr-client-id") +
                "/events/" + eventId + "/localizations";

            request.open('GET', url);
            request.send();

            request.onreadystatechange = (e) => {
                if (request.readyState !== XMLHttpRequest.DONE) {
                    return;
                }

                if (request.status !== 200) {
                    return;
                }

                let response = JSON.parse(request.responseText);

                if (response.geo_localizations && response.geo_localizations.length > 0) {
                    if (map) {
                        map.getSource('route').setData({
                            'type': 'Feature',
                            'properties': {},
                            'geometry': {
                                'type': 'LineString',
                                'coordinates': response.geo_localizations.map(l => [l.longitude, l.latitude])
                            }
                        })
                    } else {
                        showMap(response.geo_localizations)
                    }
                }
            }
        }
    }

    function assignEvent(assignButton, callback) {
        const request = new XMLHttpRequest()
        const url = "/company/" + assignButton.getAttribute("attr-company") +
            "/users/" + assignButton.getAttribute("attr-user-id") + "/assign";

        request.open('GET', url);
        request.send();

        request.onreadystatechange = (e) => {
            if (request.readyState !== XMLHttpRequest.DONE) {
                return;
            }

            if (request.status !== 200) {
                return callback(false);
            }

            const event = JSON.parse(request.responseText);
            console.log(event)

            const alertMessage = document.querySelector("#alert-id");

            // alertMessage.innerHTML = alertMessage.innerHTML.replace(/Alerta( \d*)?/, "Alerta " + event.id);

            setTimeout(() => {
                const video = document.querySelector(".recording");

                video.setAttribute('src', "https://fierce-caverns-84695.herokuapp.com/users/" +
                    event.client_id + "/events/" + event.id + "/recordings/" + event.recording_id);
                video.load();
            }, 0);

            const requestClient = new XMLHttpRequest()
            const url = "/users/" + event.client_id;

            requestClient.open('GET', url);
            requestClient.send();

            requestClient.onreadystatechange = (e) => {
                if (requestClient.readyState !== XMLHttpRequest.DONE) {
                    return;
                }

                let alarmUser = JSON.parse(requestClient.responseText);
                console.log(alarmUser);

                document.getElementById("nomYape").text = "Nombre y Apellido: " + alarmUser.first_name + " " + alarmUser.last_name;
                document.getElementById("celular").text = "Celular: " + alarmUser.area_code + " " + alarmUser.phone_number;
                document.getElementById("nroCliente").text = "Nro. Cliente: " + alarmUser.client_number;
            }

            document.querySelector(".end-event").setAttribute("attr-event-id", event.id);
            document.querySelector(".end-event").setAttribute("attr-client-id", event.client_id);

            document.querySelector(".free-event").setAttribute("attr-event-id", event.id);
            document.querySelector(".free-event").setAttribute("attr-client-id", event.client_id);

            document.getElementById("video-alert").style.visibility = "visible";
            document.getElementById("free-event-id").style.visibility = "visible";
            document.getElementById("end-event-id").style.visibility = "visible";
            document.getElementById("alert-data-id").style.visibility = "visible";

            const notFoundMessage = document.querySelector("#alert-not-found");

            if(notFoundMessage){
                notFoundMessage.style.visibility = "hidden";
            }

            document.getElementById("alertaid").text = "Alerta ID: " + event.id;
            document.getElementById("clienteID").text = "Usuario ID: " + event.client_id;

            var d = new Date(event.date);
            var formatedDate = formatDate(d);

            document.getElementById("eventDate").text = "Fecha y hora: " + formatedDate;

            showMap(event.localizations);

            document.getElementById("video-alert").style.visibility = "visible";
            document.getElementById("free-event-id").style.visibility = "visible";
            document.getElementById("end-event-id").style.visibility = "visible";
            document.getElementById("alert-data-id").style.visibility = "visible";

            return callback(true);
        }
    }
</script>
</body>
</html>

